Excellent
=========

Doe dingen met Excelbestanden zonder ze te openen.

De dingen
---------

* add: (sheet + sheet)
* append (rows)
* join (columns)
* convert to csv
* convert to json
