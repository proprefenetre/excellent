#! /usr/bin/env python

import csv
import json
from pathlib import Path

import pytest
import openpyxl

from excellent.excelfile import ExcelFile


@pytest.fixture
def sheet_from_file():
    return ExcelFile(Path(__file__).parent / "workbook.xlsx")


@pytest.fixture
def empty_sheet():
    return ExcelFile()


@pytest.fixture
def filled_sheet():
    sh = ExcelFile()
    for _ in range(10):
        sh.append(list(range(10)))
    return sh


def test_open_workbook(sheet_from_file):
    assert isinstance(sheet_from_file.workbook, openpyxl.Workbook)


def test_sheetnames(sheet_from_file):
    assert sheet_from_file.sheetnames == ["Untitled_1", "Sheet2"]


def test_get_worksheet(sheet_from_file):
    assert isinstance(
        sheet_from_file._get_active_sheet("Untitled_1"),
        openpyxl.worksheet.worksheet.Worksheet,
    )
    assert isinstance(
        sheet_from_file._get_active_sheet("Sheet2"),
        openpyxl.worksheet.worksheet.Worksheet,
    )


def test_row_header(sheet_from_file):
    first_row = ("A", "B", "C", "D", "E")
    for row in sheet_from_file.rows(skip_header=False):
        assert row == first_row
        break


def test_first_row(sheet_from_file):
    first_row = (
        0.409241674678289,
        0.607725409574877,
        0.808018673367502,
        0.430447789280929,
        0.695741664970115,
    )
    for row in sheet_from_file.rows(skip_header=True):
        assert row == first_row
        break


def test_first_row_sheet2(sheet_from_file):
    first_row = (
        0.681696115491182,
        0.846423053688345,
        0.933222645350786,
        0.904976158700165,
        0.525140340224674,
    )
    for row in sheet_from_file.rows(sheet="Sheet2", skip_header=True):
        assert row == first_row
        break


def test_column_header(sheet_from_file):
    first_col = (
        "A",
        0.409241674678289,
        0.224342327588903,
        0.223990113604701,
        0.062214894954442,
        0.053961545751028,
        0.652992098567016,
        0.19592377808202,
        0.202473977871293,
        0.825313842005187,
    )

    for col in sheet_from_file.columns(skip_header=False):
        assert col == first_col
        break


def test_first_column(sheet_from_file):
    first_col = (
        0.409241674678289,
        0.224342327588903,
        0.223990113604701,
        0.062214894954442,
        0.053961545751028,
        0.652992098567016,
        0.19592377808202,
        0.202473977871293,
        0.825313842005187,
    )

    for col in sheet_from_file.columns(skip_header=True):
        assert col == first_col
        break


def test_column_sheet2(sheet_from_file):
    first_col = (
        "A",
        0.681696115491182,
        0.755918527935957,
        0.152065555753223,
        0.198243705211581,
        0.436465901025663,
        0.956940855828613,
        0.795144105946452,
        0.722585164756499,
        0.151264917389466,
    )

    for col in sheet_from_file.columns(sheet="Sheet2", skip_header=False):
        assert col == first_col
        break


def test_new_empty_sheet(empty_sheet):
    assert isinstance(empty_sheet.workbook, openpyxl.Workbook)


def test_empty_sheet(empty_sheet):
    assert list(empty_sheet.rows()) == []


def test_filled_sheet(filled_sheet):
    assert filled_sheet._get_active_sheet().max_column == 10
    for row in filled_sheet.rows():
        assert row == tuple(range(10))


def test_append(empty_sheet):
    for _ in range(10):
        empty_sheet.append(list(range(10)))
    assert empty_sheet.dim[0] == 10


def test_add_column(empty_sheet):
    for _ in range(10):
        empty_sheet.add_column(list(range(10)))
    assert empty_sheet.dim[1] == 10


def test_join(empty_sheet):
    cols = []
    for _ in range(10):
        cols.append(range(10))
    empty_sheet.join(cols)
    assert empty_sheet.dim == (10, 10)


def test_combine_rows(filled_sheet):
    other = ExcelFile()
    for _ in range(10):
        other.append(list(range(10)))

    filled_sheet.add(other)

    assert filled_sheet.dim == (20, 10)


def test_combine_columns(filled_sheet):
    other = ExcelFile()
    for _ in range(10):
        other.append(list(range(10)))

    filled_sheet.add(other, method="columns")

    assert filled_sheet.dim == (10, 20)


def test_to_csv(filled_sheet, tmp_path):
    filled_sheet.to_csv(tmp_path / "filled_sheet.csv")

    with open(tmp_path / "filled_sheet.csv") as csvf:
        reader = csv.reader(csvf)
        for r1, r2 in zip(filled_sheet.rows(), reader):
            assert r1 == tuple(int(n) for n in r2)


def test_to_json(filled_sheet, tmp_path):
    filled_sheet.to_json(tmp_path / "filled_sheet.json")

    with open(tmp_path / "filled_sheet.json") as jsonf:
        rows = json.load(jsonf)
    
    for r1, r2 in zip(filled_sheet.rows(), rows):
        assert r1 == tuple(r2)
