#! /usr/bin/env python

import csv
import json
from pathlib import Path
from typing import Any, Iterable, List, Optional, Sequence, Tuple, Union

import openpyxl
from openpyxl import Workbook
from openpyxl.worksheet.worksheet import Worksheet


class ExcelFile:
    filename: Optional[Path]
    workbook: Workbook

    def __init__(self, filename: Optional[Path] = None) -> None:
        self.filename = filename
        self.workbook = openpyxl.load_workbook(str(filename), data_only=True) if filename else Workbook()

    def _get_active_sheet(self, sheetname: Optional[str] = None) -> Worksheet:
        if not sheetname:
            return self.workbook.active
        elif sheetname not in self.workbook.sheetnames:
            raise ValueError(f"Worksheet '{sheetname}' not in Workbook.")
        else:
            return self.workbook[sheetname]

    @property
    def dim(self, sheet: Optional[str] = None) -> Tuple[int, int]:
        """Returns the dimensions of the active sheet.

        Dimensions are (number of rows, number of columns). NB that an empty sheet has dim (1, 1).
        """
        active_sheet = self._get_active_sheet(sheet)
        return active_sheet.max_row, active_sheet.max_column

    def rows(self, sheet: Optional[str] = None, headers: bool = False) -> Iterable[Tuple[Any]]:
        active_sheet = self._get_active_sheet(sheet)
        _rows = active_sheet.iter_rows(values_only=True)
        if not headers:
            next(_rows)
        yield from _rows

    def columns(self, sheet: Optional[str] = None, headers: bool = True) -> Iterable[Tuple[Any]]:
        active_sheet = self._get_active_sheet(sheet)
        _cols = active_sheet.iter_cols(values_only=True)
        for col in _cols:
            if not headers:
                yield col[1:]
            else:
                yield col

    def add(
        self,
        other: "ExcelFile",
        method: str = "rows",
        sheet: Optional[str] = None,
        **kwargs,
    ) -> None:
        if not isinstance(other, ExcelFile):
            raise TypeError(f"Can't add object of type {type(other)} to ExcelFile.")

        if method == "rows":
            self.append(list(other.rows(headers=kwargs.get("headers", False))))
        elif method == "columns":
            self.join(list(other.columns(headers=kwargs.get("headers", True))))

    def append(
        self,
        rows: Union[Sequence[Any], Sequence[Sequence[Any]]],
        sheet: Optional[str] = None,
    ) -> None:
        active_sheet = self._get_active_sheet(sheet)
        if isinstance(rows[0], (list, tuple)):
            for row in rows:
                active_sheet.append(row)
        else:
            active_sheet.append(rows)

    def add_column(self, column: Sequence[Any], sheet: Optional[str] = None) -> None:
        active_sheet = self._get_active_sheet(sheet)

        new_col = 1 if active_sheet.cell(1, 1).value is None else active_sheet.max_column + 1

        for idx, val in zip(range(len(column)), column):
            active_sheet.cell(row=idx + 1, column=new_col, value=val)

    def join(self, columns: Sequence[Sequence[Any]], sheet: Optional[str] = None) -> None:
        for col in columns:
            self.add_column(col, sheet)

    def to_csv(
        self,
        fp: Optional[Path] = None,
        sheet: Optional[str] = None,
        headers: bool = True,
    ) -> None:
        if not fp:
            fp = self.filename.with_suffix(".csv").name

        with open(str(fp), "w", newline="", encoding="utf-8") as csvf:
            writer = csv.writer(csvf)
            for row in self.rows(sheet=sheet, headers=headers):
                writer.writerow(row)

    def to_json(
        self,
        fp: Optional[Path] = None,
        indent: int = 2,
        sheet: Optional[str] = None,
        headers: bool = True,
    ) -> None:
        if not fp:
            fp = self.filename.with_suffix(".json").name

        with open(str(fp), "w", encoding="utf-8") as jsonf:
            json.dump(
                list(self.rows(sheet=sheet, headers=headers)),
                jsonf,
                indent=indent,
            )

    def save(self, fp: Optional[Union[str, Path]] = None) -> None:
        if fp:
            self.workbook.save(str(fp))
        elif self.filename:
            self.workbook.save(self.filename.name)
        else:
            raise ValueError("Missing filename.")
