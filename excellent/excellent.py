#! /usr/bin/env python

from pathlib import Path

import click

from .excelfile import ExcelFile


@click.command()
@click.argument("src", nargs=-1, type=click.Path(path_type=Path))
@click.argument("dest", nargs=1, type=click.Path(exists=True, path_type=Path))
@click.option("-o", "--output", type=click.Path(path_type=Path), default=None)
@click.option("-h", "--headers", is_flag=True, default=False)
@click.option("-m", "--method", type=click.Choice(["rows", "columns"]), default="rows")
def run(src, dest, output, headers, method):
    sources = [ExcelFile(f) for f in src]
    dest = ExcelFile(dest)
    for f in sources:
        dest.add(f, method=method, headers=headers)

    dest.save(output)


if __name__ == "__main__":
    run()
